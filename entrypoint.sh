#!/bin/bash

# Start the run once job.
echo "Docker container has been started"
# php artisan queue:work –queue=low –tries=1 –timeout=360 –memory=256 --force
# echo "QUEUE Start"
#
# # Setup a cron schedule
# echo "* * * * * /run.sh >> /var/log/cron.log 2>&1
# */5 * * * * /usr/local/bin/php /var/www/app/artisan download:file >> /var/log/cron.log 2>&1
# */5 * * * * /usr/local/bin/php /var/www/app/artisan upload:file >> /var/log/cron.log 2>&1
# */5 * * * * /usr/local/bin/php /var/www/app/artisan program:calculate >> /var/log/cron.log 2>&1" > scheduler.txt
#
# echo "SETUP CRON DONE"

# crontab scheduler.txt
# cron -f
/usr/bin/supervisord
php -S 0.0.0.0:80 -t public
# php -S 0.0.0.0:443 -t public
