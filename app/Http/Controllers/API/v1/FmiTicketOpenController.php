<?php

namespace App\Http\Controllers\API\v1;

use App\FmiTicketOpen;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DB;

class FmiTicketOpenController extends Controller
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $key)
    {
        return $query->where('TransactionDate', 'like', '%' . $key . '%')
                    ->orWhere('BranchCode', 'like', '%' . $key . '%')
                    ->orWhere('TicketCategory', 'like', '%' . $key . '%')
                    ->orWhere('ScheduleDate', 'like', '%' . $key . '%')
                    ->orWhere('ProblemTypeCode', 'like', '%' . $key . '%')
                    ->orWhere('CustomerCode', 'like', '%' . $key . '%')
                    ->orWhere('ResponsibilityDepartmentCode', 'like', '%' . $key . '%')
                    ->orWhere('TicketDescription', 'like', '%' . $key . '%')
                    ->orWhere('RefNo', 'like', '%' . $key . '%')
                    ->orWhere('Remark', 'like', '%' . $key . '%')
                    ->orWhere('CreatedBy', 'like', '%' . $key . '%')
                    ->orWhere('CreatedDate', 'like', '%' . $key . '%')
                    ->orWhere('UpdatedBy', 'like', '%' . $key . '%')
                    ->orWhere('UpdatedDate', 'like', '%' . $key . '%');
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null)
    {
        $rules = [
            'TransactionDate' => 'nullable|date_format:Y-m-d H:i:s',
            'BranchCode' => 'nullable|max:250',
            'TicketCategory' => 'nullable|in:CRITICAL,MAJOR,MINOR,PLN',
            'ScheduleDate' => 'nullable|date_format:Y-m-d H:i:s',
            'ProblemTypeCode' => 'nullable|max:250',
            'CustomerCode' => 'nullable|max:250',
            'ResponsibilityDepartmentCode' => 'nullable|max:250',
            'TicketDescription' => 'nullable',
            'RefNo' => 'nullable|max:250',
            'Remark' => 'nullable',
            'CreatedBy' => 'nullable|max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        if (is_null($type)) {
            $rules = array_merge($rules, ['code' => 'required|max:250|unique:tic_fmi_ticket_open,code']);
        }

        return Validator::make($request->all(), $rules);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null)
    {
        if (is_null($data)) {
            $data = new FmiTicketOpen;
        }

        //if ($request->code) 
        $last_id = (new FmiTicketOpen)->select(DB::raw('COUNT(code) as lastId'))->get()->first()->lastId+1;

        if (strpos(@$request->code, 'FMI-TCK-') !== false || empty(@$request->code)){
            if(empty(@$request->code))
            {}
            else
            {$data->code = $request->code;}
        }
        else
        {
            $data->code = 'FMI-TCK-'.date('dm',strtotime($request->TransactionDate)).'-'.str_pad($last_id, 7, '0', STR_PAD_LEFT);
        }
        if ($request->TransactionDate) $data->TransactionDate = $request->TransactionDate;
        if ($request->BranchCode) $data->BranchCode = $request->BranchCode;
        if ($request->TicketCategory) $data->TicketCategory = $request->TicketCategory;
        if ($request->ScheduleDate) $data->ScheduleDate = $request->ScheduleDate;
        if ($request->ProblemTypeCode) $data->ProblemTypeCode = $request->ProblemTypeCode;
        if ($request->CustomerCode) $data->CustomerCode = $request->CustomerCode;
        if ($request->ResponsibilityDepartmentCode) $data->ResponsibilityDepartmentCode = $request->ResponsibilityDepartmentCode;
        if ($request->TicketDescription) $data->TicketDescription = $request->TicketDescription;
        if ($request->RefNo) $data->RefNo = $request->RefNo;
        if ($request->Remark) $data->Remark = $request->Remark;
        if ($request->CreatedBy) $data->CreatedBy = $request->CreatedBy;
        if ($request->CreatedDate) $data->CreatedDate = $request->CreatedDate;
        if ($request->UpdatedBy) $data->UpdatedBy = $request->UpdatedBy;
        if ($request->UpdatedDate) $data->UpdatedDate = $request->UpdatedDate;
        $data->save();

        return $data;
    }
}
