<?php

namespace App\Http\Controllers\API\v1;

use App\FmiTaskAssignment;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class FmiTaskAssignmentController extends Controller
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $key)
    {
        return $query->where('TransactionDate', 'like', '%' . $key . '%')
                    ->orWhere('BranchCode', 'like', '%' . $key . '%')
                    ->orWhere('TicketCode', 'like', '%' . $key . '%')
                    ->orWhere('ScheduleDate', 'like', '%' . $key . '%')
                    ->orWhere('MitraCode', 'like', '%' . $key . '%')
                    ->orWhere('TeamCode', 'like', '%' . $key . '%')
                    ->orWhere('ERPTaskCode', 'like', '%' . $key . '%')
                    ->orWhere('RefNo', 'like', '%' . $key . '%')
                    ->orWhere('Remark', 'like', '%' . $key . '%')
                    ->orWhere('CreatedBy', 'like', '%' . $key . '%')
                    ->orWhere('CreatedDate', 'like', '%' . $key . '%')
                    ->orWhere('UpdatedBy', 'like', '%' . $key . '%')
                    ->orWhere('UpdatedDate', 'like', '%' . $key . '%');
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null)
    {
        $rules = [
            'TransactionDate' => 'nullable|date_format:Y-m-d H:i:s',
            'BranchCode' => 'nullable|max:250',
            'TicketCode' => 'nullable|max:250',
            'ScheduleDate' => 'nullable|date_format:Y-m-d H:i:s',
            'MitraCode' => 'nullable|max:250',
            'TeamCode' => 'nullable|max:250',
            'ERPTaskCode' => 'nullable|max:250',
            'RefNo' => 'nullable|max:250',
            'Remark' => 'nullable',
            'CreatedBy' => 'nullable|max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        if (is_null($type)) {
            $rules = array_merge($rules, ['code' => 'required|max:250|unique:tic_fmi_task_assignment,code']);
        }

        return Validator::make($request->all(), $rules);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null)
    {
        if (is_null($data)) {
            $data = new FmiTaskAssignment;
        }

        if ($request->code) $data->code = $request->code;
        if ($request->TransactionDate) $data->TransactionDate = $request->TransactionDate;
        if ($request->BranchCode) $data->BranchCode = $request->BranchCode;
        if ($request->TicketCode) $data->TicketCode = $request->TicketCode;
        if ($request->ScheduleDate) $data->ScheduleDate = $request->ScheduleDate;
        if ($request->MitraCode) $data->MitraCode = $request->MitraCode;
        if ($request->TeamCode) $data->TeamCode = $request->TeamCode;
        if ($request->ERPTaskCode) $data->ERPTaskCode = $request->ERPTaskCode;
        if ($request->RefNo) $data->RefNo = $request->RefNo;
        if ($request->Remark) $data->Remark = $request->Remark;
        if ($request->CreatedBy) $data->CreatedBy = $request->CreatedBy;
        if ($request->CreatedDate) $data->CreatedDate = $request->CreatedDate;
        if ($request->UpdatedBy) $data->UpdatedBy = $request->UpdatedBy;
        if ($request->UpdatedDate) $data->UpdatedDate = $request->UpdatedDate;
        $data->save();

        return $data;
    }
}
