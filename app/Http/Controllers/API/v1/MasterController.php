<?php

namespace App\Http\Controllers\API\v1;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class MasterController extends Controller
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Display a listing of the resource.
     *
     * @param  string  $table
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function index($table, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'limit' => 'required_with:page|integer|min:1',
            'page' => 'required_with:limit|integer|min:1',
            'search' => 'nullable',
            'filter_code' => 'nullable|max:250',
            'filter_transaction_date_from' => 'required_with:filter_transaction_date_to|date_format:Y-m-d H:i:s',
            'filter_transaction_date_to' => 'required_with:filter_transaction_date_from|date_format:Y-m-d H:i:s',
            'filter_ticket_category' => 'nullable|in:CRITICAL,MAJOR,MINOR,PLN',
            'filter_customer_code' => 'nullable|max:250',
            'filter_ref_no' => 'nullable|max:250',
            'filter_customer_name' => 'nullable|max:250',
            'filter_fmi_ticket_status' => 'nullable|in:OPEN,OPC,OPM,CLOSED,POSTPONED',
            'filter_rml_ticket_status' => 'nullable|in:OPEN,OPC,OPM,CLOSED,POSTPONED',
            'filter_fmi_task_assignment_code' => 'nullable|max:250',
            'filter_fmi_task_assignment_transaction_date_from' => 'required_with:filter_fmi_task_assignment_transaction_date_to|date_format:Y-m-d H:i:s',
            'filter_fmi_task_assignment_transaction_date_to' => 'required_with:filter_fmi_task_assignment_transaction_date_from|date_format:Y-m-d H:i:s',
            'filter_fmi_task_assignment_ref_no' => 'nullable|max:250',
            'filter_fmi_task_assignment_remark' => 'nullable',
            'filter_fmi_task_assignment_customer_code' => 'nullable|max:250',
            'filter_fmi_task_assignment_customer_name' => 'nullable|max:250',
            'filter_rml_task_assignment_code' => 'nullable|max:250',
            'filter_rml_task_assignment_transaction_date_from' => 'required_with:filter_rml_task_assignment_transaction_date_to|date_format:Y-m-d H:i:s',
            'filter_rml_task_assignment_transaction_date_to' => 'required_with:filter_rml_task_assignment_transaction_date_from|date_format:Y-m-d H:i:s',
            'filter_rml_task_assignment_ref_no' => 'nullable|max:250',
            'filter_rml_task_assignment_remark' => 'nullable',
            'filter_rml_task_assignment_customer_code' => 'nullable|max:250',
            'filter_rml_task_assignment_customer_name' => 'nullable|max:250',
        ]);

        if ($validator->fails()) return makeResponse(400, 'error', 'validation is invalid', $validator->errors()->all());

        $data = getModelName($table)::when($request->search, function ($query, $key) use ($table) {
                    return getControllerName($table)::searchQuery($query, $key);
                })
                ->when($request->filter_code, function ($query, $key) {
                    return $query->where('code', $key);
                })
                ->when($request->filter_transaction_date_from && $request->filter_transaction_date_to, function ($query) use ($request) {
                    return $query->whereBetween('TransactionDate', [$request->filter_transaction_date_from, $request->filter_transaction_date_to]);
                })
                // created by Septian Ramadhan - itdev.septian@gmail.com
                ->when($request->filter_ticket_category, function ($query, $key) {
                    return $query->leftJoin('tic_fmi_ticket_status', 'tic_fmi_ticket_open.Code', '=' ,'tic_fmi_ticket_status.TicketCode')
                                ->leftJoin('mst_customer_erp', 'mst_customer_erp.Code', '=', 'tic_fmi_ticket_open.CustomerCode')
                                ->leftJoin('mst_department', 'mst_department.Code', '=','tic_fmi_ticket_open.ResponsibilityDepartmentCode')
                                ->select('tic_fmi_ticket_open.*',
                                'mst_customer_erp.Name',
                                'tic_fmi_ticket_status.TicketStatus',
                                'mst_department.Name as DepartmentName'
                                )
                                ->where('tic_fmi_ticket_open.TicketCategory', $key);
                })
                ->when($request->filter_not_equal_ticket_category, function ($query, $key) {
                  return $query->leftJoin('tic_fmi_ticket_status', 'tic_fmi_ticket_open.Code', '=' ,'tic_fmi_ticket_status.TicketCode')
                              ->leftJoin('mst_customer_erp', 'mst_customer_erp.Code', '=', 'tic_fmi_ticket_open.CustomerCode')
                              ->leftJoin('mst_department', 'mst_department.Code', '=','tic_fmi_ticket_open.ResponsibilityDepartmentCode')
                              ->select('tic_fmi_ticket_open.*',
                              'mst_customer_erp.Name',
                              'tic_fmi_ticket_status.TicketStatus',
                              'mst_department.Name as DepartmentName'
                              )
                              ->where('tic_fmi_ticket_open.TicketCategory', '!=', $key);
                })
                // end created by Septian Ramadhan - itdev.septian@gmail.com
                ->when($request->filter_customer_code, function ($query, $key) {
                    return $query->where('CustomerCode', $key);
                })
                ->when($request->filter_ref_no, function ($query, $key) {
                    return $query->where('RefNo', $key);
                })
                ->when($request->filter_customer_name, function ($query, $key) {
                    return $query->join('mst_customer_erp', 'CustomerCode', '=', 'mst_customer_erp.Code')
                                ->where('mst_customer_erp.Name', $key);
                })
                ->when($request->filter_fmi_ticket_status, function ($query, $key) {
                    return $query->join('tic_fmi_ticket_status', 'tic_fmi_ticket_open.code', '=', 'tic_fmi_ticket_status.TicketCode')
                                ->select('tic_fmi_ticket_open.*')
                                ->where('tic_fmi_ticket_status.TicketStatus', $key);
                })
                ->when($request->filter_rml_ticket_status, function ($query, $key) {
                    return $query->join('tic_rml_ticket_status', 'tic_rml_ticket_open.code', '=', 'tic_rml_ticket_status.TicketCode')
                                ->select('tic_rml_ticket_open.*')
                                ->where('tic_rml_ticket_status.TicketStatus', $key);
                })
                ->when($request->filter_fmi_task_assignment_code, function ($query, $key) {
                    return $query->where('tic_fmi_task_assignment.code', $key);
                })
                ->when($request->filter_fmi_task_assignment_transaction_date_from && $request->filter_fmi_task_assignment_transaction_date_to, function ($query) use ($request) {
                    return $query->whereBetween('tic_fmi_task_assignment.TransactionDate', [$request->filter_fmi_task_assignment_transaction_date_from, $request->filter_fmi_task_assignment_transaction_date_to]);
                })
                ->when($request->filter_fmi_task_assignment_ref_no, function ($query, $key) {
                    return $query->where('tic_fmi_task_assignment.RefNo', $key);
                })
                ->when($request->filter_fmi_task_assignment_remark, function ($query, $key) {
                    return $query->where('tic_fmi_task_assignment.Remark', $key);
                })
                ->when($request->filter_fmi_task_assignment_customer_code, function ($query, $key) {
                    return $query->join('tic_fmi_ticket_open', 'tic_fmi_task_assignment.TicketCode', '=', 'tic_fmi_ticket_open.code')
                                ->select('tic_fmi_task_assignment.*')
                                ->where('tic_fmi_ticket_open.CustomerCode', $key);
                })
                ->when($request->filter_fmi_task_assignment_customer_name, function ($query, $key) {
                    return $query->join('tic_fmi_ticket_open', 'tic_fmi_task_assignment.TicketCode', '=', 'tic_fmi_ticket_open.code')
                                ->join('mst_customer_erp', 'tic_fmi_ticket_open.CustomerCode', '=', 'mst_customer_erp.Code')
                                ->select('tic_fmi_task_assignment.*', 'tic_fmi_ticket_open.CustomerCode')
                                ->where('mst_customer_erp.Name', $key);
                })
                ->when($request->filter_rml_task_assignment_code, function ($query, $key) {
                    return $query->where('tic_rml_task_assignment.code', $key);
                })
                ->when($request->filter_rml_task_assignment_transaction_date_from && $request->filter_rml_task_assignment_transaction_date_to, function ($query) use ($request) {
                    return $query->whereBetween('tic_rml_task_assignment.TransactionDate', [$request->filter_rml_task_assignment_transaction_date_from, $request->filter_rml_task_assignment_transaction_date_to]);
                })
                ->when($request->filter_rml_task_assignment_ref_no, function ($query, $key) {
                    return $query->where('tic_rml_task_assignment.RefNo', $key);
                })
                ->when($request->filter_rml_task_assignment_remark, function ($query, $key) {
                    return $query->where('tic_rml_task_assignment.Remark', $key);
                })
                ->when($request->filter_rml_task_assignment_customer_code, function ($query, $key) {
                    return $query->join('tic_rml_ticket_open', 'tic_rml_task_assignment.TicketCode', '=', 'tic_rml_ticket_open.code')
                                ->select('tic_rml_task_assignment.*')
                                ->where('tic_rml_ticket_open.CustomerCode', $key);
                })
                ->when($request->filter_rml_task_assignment_customer_name, function ($query, $key) {
                    return $query->join('tic_rml_ticket_open', 'tic_rml_task_assignment.TicketCode', '=', 'tic_rml_ticket_open.code')
                                ->join('mst_customer_erp', 'tic_rml_ticket_open.CustomerCode', '=', 'mst_customer_erp.Code')
                                ->select('tic_rml_task_assignment.*', 'tic_rml_ticket_open.CustomerCode')
                                ->where('mst_customer_erp.Name', $key);
                })
                ->paginate($request->limit);

        return makeResponse(200, 'pagination', null, getResourceName($table)::collection($data));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Store a newly created resource in database.
     *
     * @param  string  $table
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function store($table, Request $request)
    {
        $validator = getControllerName($table)::validation($request);

        if ($validator->fails()) return makeResponse(400, 'error', 'validation is invalid', $validator->errors()->all());

        $data = getControllerName($table)::save($request);

        $resource = getResourceName($table);

        Log::info('[' . $data->getTable() . '.create] ' . json_encode(array_merge(['ClientIP' => $request->ip()], ['Data' => new $resource($data)])));

        return makeResponse(201, 'success', 'new ' . str_replace('-', ' ', $table) . ' has been save successfully', new $resource($data));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Display the specified resource.
     *
     * @param  string  $table
     * @param  string  $id
     * @return json
     */
    public function show($table, $id)
    {
        $data = getModelName($table)::find(str_replace('%20', ' ', $id));

        if (!$data) return makeResponse(404, 'error', str_replace('-', ' ', $table) . ' not found');

        $resource = getResourceName($table);

        return makeResponse(200, 'success', null, new $resource($data));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Update the specified resource in database.
     *
     * @param  string  $table
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $id
     * @return json
     */
    public function update($table, Request $request, $id)
    {
        $data = getModelName($table)::find(str_replace('%20', ' ', $id));

        if (!$data) return makeResponse(404, 'error', str_replace('-', ' ', $table) . ' not found');

        $validator = getControllerName($table)::validation($request, 'update');

        if ($validator->fails()) return makeResponse(400, 'error', 'validation is invalid', $validator->errors()->all());

        $resource = getResourceName($table);

        Log::info('[' . $data->getTable() . '.edit] ' . json_encode(array_merge(['ClientIP' => $request->ip()], ['OldData' => new $resource($data), 'NewData' => array_merge([$data->getKeyName() => str_replace('%20', ' ', $id)], $request->all())])));

        $data = getControllerName($table)::save($request, $data);

        return makeResponse(200, 'success', str_replace('-', ' ', $table) . ' has been update successfully', new $resource($data));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Remove the specified resource from database.
     *
     * @param  string  $table
     * @param  string  $id
     * @return json
     */
    public function destroy($table, $id)
    {
        $data = getModelName($table)::find(str_replace('%20', ' ', $id));

        if (!$data) return makeResponse(404, 'error', str_replace('-', ' ', $table) . ' not found');

        $resource = getResourceName($table);

        Log::info('[' . $data->getTable() . '.delete] ' . json_encode(array_merge(['ClientIP' => request()->ip()], ['Data' => new $resource($data)])));

        $data->delete();

        return makeResponse(200, 'success', str_replace('-', ' ', $table) . ' has been delete successfully');
    }
}
