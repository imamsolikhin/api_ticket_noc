<?php

namespace App\Http\Controllers\API\v1;

use App\FmiTicketStatus;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class FmiTicketStatusController extends Controller
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $key)
    {
        return $query->where('TransactionDate', 'like', '%' . $key . '%')
                    ->orWhere('BranchCode', 'like', '%' . $key . '%')
                    ->orWhere('TicketCode', 'like', '%' . $key . '%')
                    ->orWhere('TicketStatus', 'like', '%' . $key . '%')
                    ->orWhere('RefNo', 'like', '%' . $key . '%')
                    ->orWhere('Remark', 'like', '%' . $key . '%')
                    ->orWhere('CreatedBy', 'like', '%' . $key . '%')
                    ->orWhere('CreatedDate', 'like', '%' . $key . '%')
                    ->orWhere('UpdatedBy', 'like', '%' . $key . '%')
                    ->orWhere('UpdatedDate', 'like', '%' . $key . '%');
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null)
    {
        $rules = [
            'TransactionDate' => 'nullable|date_format:Y-m-d H:i:s',
            'BranchCode' => 'nullable|max:250',
            'TicketCode' => 'nullable|max:250',
            'TicketStatus' => 'nullable|in:OPEN,OPC,OPM,CLOSED,POSTPONED',
            'RefNo' => 'nullable|max:250',
            'Remark' => 'nullable',
            'CreatedBy' => 'nullable|max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        if (is_null($type)) {
            $rules = array_merge($rules, ['code' => 'required|max:250|unique:tic_fmi_ticket_status,code']);
        }

        return Validator::make($request->all(), $rules);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null)
    {
        if (is_null($data)) {
            $data = new FmiTicketStatus;
        }

        if ($request->code) $data->code = $request->code;
        if ($request->TransactionDate) $data->TransactionDate = $request->TransactionDate;
        if ($request->BranchCode) $data->BranchCode = $request->BranchCode;
        if ($request->TicketCode) $data->TicketCode = $request->TicketCode;
        if ($request->TicketStatus) $data->TicketStatus = $request->TicketStatus;
        if ($request->RefNo) $data->RefNo = $request->RefNo;
        if ($request->Remark) $data->Remark = $request->Remark;
        if ($request->CreatedBy) $data->CreatedBy = $request->CreatedBy;
        if ($request->CreatedDate) $data->CreatedDate = $request->CreatedDate;
        if ($request->UpdatedBy) $data->UpdatedBy = $request->UpdatedBy;
        if ($request->UpdatedDate) $data->UpdatedDate = $request->UpdatedDate;
        $data->save();

        return $data;
    }
}
