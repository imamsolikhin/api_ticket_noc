<?php

namespace App\Http\Controllers\API\v1;

use App\RmlTicketOpenShaper;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RmlTicketOpenShaperController extends Controller
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $key)
    {
        return $query->where('HeaderCode', 'like', '%' . $key . '%')
                    ->orWhere('ShaperCode', 'like', '%' . $key . '%')
                    ->orWhere('CreatedBy', 'like', '%' . $key . '%')
                    ->orWhere('CreatedDate', 'like', '%' . $key . '%')
                    ->orWhere('UpdatedBy', 'like', '%' . $key . '%')
                    ->orWhere('UpdatedDate', 'like', '%' . $key . '%');
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null)
    {
        $rules = [
            'HeaderCode' => 'nullable|max:250',
            'ShaperCode' => 'nullable|max:250',
            'CreatedBy' => 'nullable|max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        if (is_null($type)) {
            $rules = array_merge($rules, ['code' => 'required|max:250|unique:tic_rml_ticket_open_shaper,code']);
        }

        return Validator::make($request->all(), $rules);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null)
    {
        if (is_null($data)) {
            $data = new RmlTicketOpenShaper;
        }

        if ($request->code) $data->code = $request->code;
        if ($request->HeaderCode) $data->HeaderCode = $request->HeaderCode;
        if ($request->ShaperCode) $data->ShaperCode = $request->ShaperCode;
        if ($request->CreatedBy) $data->CreatedBy = $request->CreatedBy;
        if ($request->CreatedDate) $data->CreatedDate = $request->CreatedDate;
        if ($request->UpdatedBy) $data->UpdatedBy = $request->UpdatedBy;
        if ($request->UpdatedDate) $data->UpdatedDate = $request->UpdatedDate;
        $data->save();

        return $data;
    }
}
