<?php

namespace App\Http\Middleware;

use Closure;

class APIMiddleware
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      return $next($request);
        if ($request->header('Accept')) {
            if ($request->header('Accept') == 'application/json') {
                return $next($request);
            }

            return makeResponse(406, 'error', 'not acceptable');
        }

        return makeResponse(400, 'error', 'bad request');
    }
}
