<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FmiTicketStatus extends JsonResource
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Transform the resource into an array.
     *
     * @param  Illuminate\Database\Eloquent\Model  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'code' => $this->code,
            'TransactionDate' => $this->TransactionDate != null ? date('Y-m-d H:i:s', strtotime($this->TransactionDate)) : null,
            'BranchCode' => $this->BranchCode,
            'TicketCode' => $this->TicketCode,
            'TicketStatus' => $this->TicketStatus,
            'RefNo' => $this->RefNo,
            'Remark' => $this->Remark,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
        ];
    }
}