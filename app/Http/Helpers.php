<?php

/**
 * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
 * Make a Response.
 *
 * @param  int  $statusCode
 * @param  string  $status
 * @param  string  $message
 * @param  array|object|null  $data
 * @param  array  $headers
 * @return json
 */
function makeResponse($statusCode, $status, $message, $data = null, $headers = [])
{
    $result = [
        'status_code' => $statusCode,
        'status' => $status == 'pagination' ? 'success' : $status,
        'message' => $message,
        'data' => $data,
    ];

    if ($status == 'pagination') {
        $result = array_merge($result, ['paginator' => [
            'total_records' => (int) $data->total(),
            'total_pages' => (int) $data->lastPage(),
            'current_page' => (int) $data->currentPage(),
            'per_page' => (int) $data->perPage(),
        ]]);
    }

    return response()->json($result, $statusCode, $headers);
}

/**
 * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
 * Make a Key Name.
 *
 * @param  string  $table
 * @return string
 */
function getKeyName($table)
{
    return str_replace(' ', '', ucwords(str_replace('-', ' ', $table)));
}

/**
 * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
 * Make a Model Name.
 *
 * @param  string  $table
 * @return string
 */
function getModelName($table)
{
    return 'App\\' . getKeyName($table);
}

/**
 * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
 * Make a Resource Name.
 *
 * @param  string  $table
 * @return string
 */
function getResourceName($table)
{
    return 'App\Http\Resources\\' . getKeyName($table);
}

/**
 * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
 * Make a Controller Name.
 *
 * @param  string  $table
 * @return string
 */
function getControllerName($table)
{
    return 'App\Http\Controllers\API\v1\\' . getKeyName($table) . 'Controller';
}
