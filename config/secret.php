<?php

/*
* Andrea Adam | andreaadam215@gmail.com | mrandreid.com
* Set The Secrets
*/
return [
    /*
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Public Key (string)
     */
    'public_key' => env('PUBLIC_KEY_SECRET'),
];